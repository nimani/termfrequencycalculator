
####This is a simple program for calculating Term Frequency
It takes as input:

    1-a set of documents  
    2-a list of words
    
Returns:
 
 the document with the highest TF score for each word 
and the TF score for that word in that document

#### How to run:
Assuming you have git and sbt installed

- git clone https://nimani@bitbucket.org/nimani/termfrequencycalculator.git

- Place the documents in src/man/resources directory 
(Sample documents included - Assuming files have .txt extention but it can easily changed for other types)

- Run with command: sbt "run word1 word2 word3 ..." for calulating TF 
(Default case: sbt run   By default it runs the TF for  the words “queequeg”, “whale”, and “sea” )

####Output:

- code creates a csv file in target direcory containing the result



- In consule it will print the result with the following format 

<h5>Word | TermFrequency | DocumentName
-----------
<h5>queequeg | 0.006610576923076923 | mobydick-chapter4.txt
----
<h5>sea | 0.004486316733961417 | mobydick-chapter1.txt
-------------------------
<h5>whale | 0.0013458950201884253 | mobydick-chapter1.txt
-----------------------------


#### For next revisions
- The code can easily modified to write the result to a file based on given output path (given in a config file or as an argument) but it is not included for simplicity.
- Tests are not included but could be added
- Spark is being ran loacaly with as many as number of cores but it can be modified
- the code can be more modulized to have a separate class for running the job
