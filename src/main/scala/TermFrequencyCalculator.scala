import java.io.{File, PrintWriter}

import org.apache.spark.SparkContext._
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Imanima
  */
object TermFrequencyCalculator {

  def main(args: Array[String]): Unit = {

    val selectedWords = if (args.isEmpty) Seq("queequeg", "whale", "sea") else args.toSeq
    val resourceFolder = new File(getClass.getResource("").getPath)
    val filePathList = if (resourceFolder.exists && resourceFolder.isDirectory)
      resourceFolder.listFiles.filter(_.getName.endsWith(".txt"))
    else throw new Exception(s"Could not find any file to process in the resources folder with path  ${resourceFolder.getAbsolutePath}")

    // Rdd of fileName -> Rdd(word-> TermFrequency )
    val fnameTfRdd = filePathList.map(file => (file.getName, calculateTf(file.getPath, selectedWords)))

    // Transform and flatten rdd to word -> (TF ,fileName) , reduce it by picking the max TF per word
    val transformedMap = fnameTfRdd.map { case (fName, tfList) =>
      tfList.map(tf => tf._1 -> (tf._2, fName))
    }.reduce(_.union(_)).reduceByKey((tf1, tf2) => if (tf1._1 > tf2._1) tf1 else tf2).collect()
    sc.stop()

    //write to the file in resource folder  named the (combination of words).result
    val resultPw = new PrintWriter(new File(selectedWords.mkString("_") + ".csv"))
    resultPw.write("Word,TermFrequency,DocumentName \n")
    transformedMap.foreach(w => resultPw.write(s"${w._1},${w._2._1},${w._2._2}\n"))
    resultPw.close()

    //print to consule
    println(s"Word  , TermFrequency , DocumentName")
    transformedMap.foreach(w => println(s"${w._1} , ${w._2._1} , ${w._2._2}"))
  }

  private val conf = new SparkConf().
    setAppName("TermFrequencyCalculator").
    setMaster("local[*]")
  private val sc = new SparkContext(conf)


  //returns map of words with their count for a given file
  private def calculateTf(filePath: String, targetWords: Seq[String]) = {

    val textRdd = sc.textFile(filePath)
    val words = textRdd.flatMap(line => tokenize(line))
    val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)
    val totalWordCount = wordCounts.values.sum
    if (totalWordCount < 1) throw new NoSuchElementException(s"Could not find any words in file with Path $filePath") // this could be only logging error depanding on use case

    //only return the TF for target words as Rdd[(word , Tf)]
    val filteredWordsCount = wordCounts.filter(x => targetWords.contains(x._1)).map(x => (x._1, x._2 / totalWordCount))
    filteredWordsCount
  }

  private def tokenize(line: String): Seq[String] = {
    line.toLowerCase.replaceAll("[^a-zA-Z0-9\\s]", "").split(" ")
  }
}
